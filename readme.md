# kconfiglib_tool

![kcfg3](https://gitee.com/nikolan/pic-bed/raw/master/kcfg3.jpg)

开源地址:https://gitee.com/nikolan/kconfiglib_tool

kconfiglib_tool工具实现宏定义配置图形化选项菜单，
能让你在Windows平台下使用kconifg，来管理你的系统配置选项，
适用于为各种单片机宏定义配置图形化选项菜单。

kconifg是一种配置文件的语法<br>
如果有接触过esp-idf或者是Linux的menuconfig，相信就对这个比较熟悉。<br>

这个工具最终目的是通过简单的命令行字符图形界面<br>
来方便地配置各种选项最终生成一个头文件的各种宏定义<br>
这些宏定义最终会间接影响编译生成的固件<br>
通过这个可以方便地裁剪功能或者选择不同的配置<br>


而近年来越来越多厂商提供的SDK，<br>
都是以kconfig来实现的menuconfig来做一些配置。<br>

例如:

- esp-idf
- Zephyr
- rt-thread-env
- NuttX
  
# 使用方法

## 基本测试

这个kconfig.exe是使用了pyinstaller打包的exe可执行文件<br>
理论上是没有pyhton环境的电脑也可以打开使用<br>
不过我也没法测试<br>

所以在放到你工程上使用前可以先跑一下项目我测试的这个demo<br>

直接双击打开kconfig.exe会重新menuconfig类似的界面<br>
![kcfg1](https://gitee.com/nikolan/pic-bed/raw/master/kcfg1.jpg)

然后随便修改一个选项<br>
![kcfg2](https://gitee.com/nikolan/pic-bed/raw/master/kcfg2.jpg)

按q退出时会询问是否保存修改<br>
选Yes<br>
![kcfg3](https://gitee.com/nikolan/pic-bed/raw/master/kcfg3.jpg)

接着查看生成的这个gen_config.h里面的宏定义的值<br>
是否和你在界面修改的一致。<br>
![kcfg4](https://gitee.com/nikolan/pic-bed/raw/master/kcfg4.jpg)


## 放到工程上使用

1.首先使用kconfig的语法来<br>
编写对应配置菜单文件<br>
conifg.in<br>
如果你对kconfig一点都不懂<br>
就看我的demo改字符串就行<br>
后面再看看语法,都比较简单<br>

![kcfg5](https://gitee.com/nikolan/pic-bed/raw/master/kcfg5.jpg)


2.复制 kconfig.exe<br>
到你工程的tool工具目录下<br>

3.然后复制user.json<br>
到kconfig.exe的同一目录<br>
填写必要的配置<br>
填写无误后直接运行就能生成gen_config.h<br>
![kcfg7](https://gitee.com/nikolan/pic-bed/raw/master/kcfg7.png)


4.以下对json文件的配置项做详细说明<br>
其实如果你没有什么要求只需要修改下<br>
input_file_path<br>
output_file_path<br>
include_libs<br>
这三项就可以了,其他没什么必要改，只是留出来接口给大家玩玩<br>

![kcfg6](https://gitee.com/nikolan/pic-bed/raw/master/kcfg6.jpg)

- input_file_path": "./config.in"<br>
填写kconfig的菜单描述文件.in所在目录<br>

- "output_file_path": "./gen_config.h"<br> 
  填写最终生成的.h头文件配置的目录<br>

- "defconfig_path":"./defconfig.config",<br>
  填写初步处理的配置项文件的目录

- "defconfig_header":"./defconfig.h",<br>
  填写按配置项文件转为宏定义后的初步头文件目录

- "update_time": true,<br>
  是否开启每次配置后更新头文件的生成配置的时间，默认开启<br>

- "add_extern_c": false,<br>
  是否给头文件加入extern c语法使得c++可兼容使用，默认开启<br>

- "fg_color":"white",<br>
  终端字体的颜色<br>

- "bg_color":"green",<br>
  终端背景的颜色<br>

- "encoding":"utf-8",<br>
  字体编码UTF-8,好像如果有非英文字体不用UTF-8会报错<br>
    
- "creator":"your name here",<br>
   配置的作者名称<br>
  
- "first_letter_of_macro":"#",<br>
  字符串首字符为#时，取消字符串双引号，只取字符串名称<br>

- "item_prefix":"CONFIG_",<br>
  生成的配置项宏定义的前缀，好像Linux都是加这个前缀的<br>

- "description":[<br>
       "your Description!"<br>
    ],<br>
    添加头文件的描述，这里可以写自己的一些说明注意事项什么的<br>

- "include_libs": [<br>
<br>
      "#include \"stdint.h\"",<br>
      "#include \"stdbool.h\"",<br>
      "#include \"stm32f4xx.h\""<br>
<br>
    ]<br>
    添加需要引入到生成的配置头文件的各种头文件和库<br>
    注意这些双引号在字符串内需要使用\",以逗号分开每一项<br>
<br>

# 二次开发添加的功能

这个和kconfig的menuconfig的功能<br>
基本变化不大，语法也是和kconfig一样<br>
就稍微添加了2个功能方便配置.<br>

## 通过json文件来配置程序
![kcfg6](https://gitee.com/nikolan/pic-bed/raw/master/kcfg6.jpg)
因为直接打包为exe<br>
方便没有python和各种库<br>
都能直接运行在电脑上<br>
而且需要配置的功能比较多<br>
通过命令行传参比较麻烦<br>

干脆就用一个user.json<br>
来填写各种参数程序来读取这个文件<br>



## 字符串以#开头则二次处理取消字符串的双引号
![kcfg7](https://gitee.com/nikolan/pic-bed/raw/master/kcfg7.png)
这个主要是为了能直接填写宏定义的名称和变量名称<br>
因为kconfig的变量类型不能直接填名称<br>

但是我希望配置一下底层的HAL库的结构体<br>
某些选项就需要填宏定义的名称，<br>
总不能填地址或者去库翻一翻宏定义是什么数值吧<br>
这样配置的话可读性也太差了。<br>

所以参考就添加这么个方法把以#开头的字符串<br>
直接填字符而不加双引号<br>

利用这个方法,如果你和我一样<br>
其实并不需要使用kconfig的其他复杂功能<br>
只需要能用图形配置菜单填写宏定义的值<br>
那么甚至可以简化kconfig的语法<br>
把几乎所有选项全变为字符串类型<br>

只需要会用这个格式<br>
就能轻松自定义设计menuconfig的菜单了<br>

``` c

menu "menu_name"
config A
	string "A"
	default "#xxx"
endmenu

```

如果你希望这个宏定义A是整数1<br>
default "#1"就直接处理为1<br>
<br>
如果你希望它是浮点数<br>
default "#1.2"直接处理为1.2<br>
<br>
如果你希望直接填宏定义或变量名称<br>
default "#GPIOA"直接处理为GPIOA<br>
<br>
如果你就需要它是字符串则不以#开头<br>
default "string"处理为"string"<br>
<br>
当然这个是pyhton二次处理的,不是kconfig的标准语法。<br>
直接用kconfig也没有什么难度<br>
这个处理只有你字符串不使用#开头语法也不冲突<br>


# 后续 TODO

<br>
最初这个东西是运行于Linux的<br>
有看到移植到window的版本<br>


python的版本：<br>
是现在这个Kconfiglib<br>
https://github.com/ulfalizer/Kconfiglib<br>

c的版本：<br>
是这个kconfig-frontends<br>
官方原版：https://github.com/uvc-ingenieure/kconfig-frontends<br>
乐鑫fork的版本：https://github.com/espressif/kconfig-frontends<br>

这里使用python版本进行重新封装开发一下<br>
c版本的因为cmake报错没找到解决办法<br>
安装了一堆gunwin32的各种依赖库<br>
cmake终于生成Makefile了<br>
后面make都到100%<br>
连接为exe文件时又说有什么定义找不到了编译失败<br>
搞了一天了没办法最后还是选择用python这个<br>


![kcfg8](https://gitee.com/nikolan/pic-bed/raw/master/kcfg8.png)

在python的这个kconfiglib<br>
我有尝试了使用中文字符来做配置菜单<br>
在普通cmd终端下会重新中文文字重叠<br>
换一个比较好的终端切换界面后会有中文字体的残留<br>

猜测是底层对中文字符串不太支持,<br>
应该是删除时，为了速度没有全刷而是局刷<br>
但是没考虑中文的宽度，<br>
pyhton的库也没法查查源码排查就放弃做中文支持了。<br>

如果使用C语言版本的说不定是能查查代码改一下API的<br>
尝试解决字体残留的<br>
但是C语言版本的编译不过<br>
就先这样了<br>

___ageek一个极客开源<br>


## 二次开发环境搭建

请安装pyhton3
然后通过pip install 安装kconfig.py需要用到的各种库
例如
``` 

pip install kconfiglib
pip install pyinstaller

```
之后直接

```

python ./kconfig

```
进行调试

最终通过pyinstaller来打包为exe
这样exe就不需要python环境单独执行了
打包完成后的exe在./dist下

```
pyinstaller ./kconfig.py

```