// last update time: 2023-11-07 17:45:04.183300
// creator: your name here
/*
*your Description!
*/
#ifndef GEN_CONFIG_H
#define GEN_CONFIG_H

#include "stdint.h"
#include "stdbool.h"
#include "stm32f4xx.h"


//STATE------------------------------------------------------------------------------------------------------------------
//config_state_led_gpio
#define CONFIG_STATE_LED_GPIO GPIOF
//config_state_led_pin
#define CONFIG_STATE_LED_PIN GPIO_Pin_13
//config_state_led_on_polarity
#define CONFIG_STATE_LED_ON_POLARITY 0
//config_state_led_clk
#define CONFIG_STATE_LED_CLK RCC_AHB1Periph_GPIOC
//config_state_led_clk_cmd
#define CONFIG_STATE_LED_CLK_CMD RCC_AHB1PeriphClockCmd

//DELAY------------------------------------------------------------------------------------------------------------------
//config_delay_tim
#define CONFIG_DELAY_TIM TIM10
//config_delay_clk
#define CONFIG_DELAY_CLK RCC_APB2Periph_TIM10
//config_delay_clk_cmd
#define CONFIG_DELAY_CLK_CMD RCC_APB2PeriphClockCmd

#endif//GEN_CONFIG_H
